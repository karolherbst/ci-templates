# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

# Note, we need to include fedora.yml for building our qemu initial images
# however, the CI script already includes it, so we can not pull it here.
#
# include:
#   - local: '/templates/fedora.yml'

#################################################################
#                                                               #
#                    bootstrapping stage                        #
#                                                               #
#################################################################


.bootstrap_skeleton:
  extends: .fdo.container-build@fedora
  stage: bootstrapping
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
    FDO_DISTRIBUTION_VERSION: '32'
    FDO_DISTRIBUTION_TAG: '{{bootstrap_tag}}'


# we need a minimalist image capable of buildah, podman, skopeo, curl,
# jq, date and test. We used to rely on `bootstrap/bootstrap.sh`, but
# a commit in runc prevented it to be compiled against musl. So we just
# end up building a regular container image from arch.
.bootstrap:
  extends: .bootstrap_skeleton
  image: fedora:32
  before_script:
    - bash bootstrap/bootstrap.sh
  variables:
    FDO_REPO_SUFFIX: buildah
    FDO_DISTRIBUTION_EXEC: 'bash bootstrap/bootstrap.sh'


.bootstrap@arm64v8:
  extends: .bootstrap
  image: arm64v8/fedora:latest
  tags:
    - aarch64
  variables:
    FDO_REPO_SUFFIX: arm64v8/buildah


# qemu container capable of running a VM to run the test suite
#
# installed required packages (in addition to the bootstrap ones):
# - qemu (of course)
# - genisoimage (to create a cloud-init iso that will help us filling in the custom parameters)
# - usbutils (for being able to call lsusb and redirect part a USB device)
.qemu:
  extends: .bootstrap_skeleton
  image: $CI_REGISTRY_IMAGE/buildah:{{bootstrap_tag}}
  stage: bootstrapping_qemu
  dependencies: []
  variables:
    FDO_DISTRIBUTION_TAG: '{{qemu_tag}}'
    FDO_BASE_IMAGE: $CI_REGISTRY_IMAGE/buildah:{{bootstrap_tag}}
    FDO_REPO_SUFFIX: qemu-base
    FDO_DISTRIBUTION_PACKAGES: 'qemu genisoimage usbutils'
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl.sh /app/vmctl'

# qemu container capable of creating an other VM image
#
.qemu-mkosi:
  extends: .qemu
  tags:
    - kvm
  variables:
    FDO_REPO_SUFFIX: qemu-mkosi-base
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl.sh /app/vmctl && bootstrap/prep_mkosi.sh'
